public class Task2 {
    public static void main(String[] args) {
        if (args.length==0) {
            System.out.println("Sorry mate, you gotta add an argument");
            return;
        }
        System.out.println("Hello "+args[0]+", your name is "+args[0].length()+" characters long and start with a "+args[0].charAt(0));
        if (args.length>1) {
            System.out.println("Trying to be tricky, eh? Sorry Craig, this program only cares about your first argument");
        }
    }
}